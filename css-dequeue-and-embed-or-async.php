<?php
/**
 * Plugin Name: CSS dequeue and embed or async
 * Plugin URI:  https://bitbucket.org/austinjp/css-dequeue-and-embed-or-async
 * Description: Take control of CSS that plugins etc put into your pages. Decide how you want Wordpress to utilise that CSS.
 * Version:     0.0.1
 * Author:      Austin Plunkett
 * Author URI:  https://bitbucket.org/austinjp
 * Text Domain: cdea
 * License:     GNU General Public License v2.0 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * 
 * Inspired by https://github.com/NateWr/asset-queue-manager
 */

// Prevent direct access
if (!defined('ABSPATH')) { exit; }

global $wp_styles, $enq_styles;

if ( is_admin() ){
    add_action('admin_menu', '_admin_menu');
    function _admin_menu() {
        add_options_page('CSS dequeue embed async', 'CSS dequeue embed async', 'manage_options', 'cdae_options', '_admin_html');
    }
} else {
    // https://stackoverflow.com/documentation/wordpress/9699/get-home-path
    require_once(ABSPATH . 'wp-admin/includes/file.php');
}


function dequeue_styles() {
    global $wp_styles, $enq_styles;
    $enq_styles = array();
    foreach($wp_styles->queue as $style) {
	if (local_stylesheet($wp_styles->registered[$style]->src)) {
	    $enq_styles[] = $wp_styles->registered[$style]->src;
	    wp_deregister_style($style);
	    wp_dequeue_style($style);
	}
    }
}
add_action('wp_enqueue_scripts', 'dequeue_styles', 25);


function local_stylesheet($s) {
    // FIXME This is pretty janky.
    // Takes a URL for a stylesheet and returns either
    // a string (the path to the stylesheet on disk) or
    // false (if the stylesheet is hosted elsewhere e.g. Google Fonts etc)
    $basedir = get_home_path();
    $uri = get_site_url();
    $uri = preg_replace('/\//', '\/', $uri);
    $s = preg_replace('/' . $uri . '/', $basedir, $s);

    // If a protocol still remains, assume the stylesheet is from another server.
    if (preg_match('/^[^:]+\:\/\//', $s)) {
	return false;
    }
    $s = preg_replace('/\?.*/', '', $s);
    if ($s) {
	return $s;
    }
    return false;
}


function add_css() {
    global $enq_styles;
    wp_enqueue_style('cdae', plugin_dir_url(__FILE__). 'assets/css/style.css');
    if ($enq_styles) {
	foreach ($enq_styles as $f) {
	    $f = local_stylesheet($f);
	    if ($f && is_readable($f)) {
		$css = file_get_contents($f);
		wp_add_inline_style('cdae', $css);
	    }
	}
    }
}
add_action('wp_enqueue_scripts','add_css', 30);


// This removes the HTML "<link rel='stylesheet' id='cdae-css' href='...'>" from the
// page. It's not necessary since this plugin does not use a stylesheet. However, it
// does register a stylesheet in order to slurp-in CSS from all other plugins. By
// registering a stylesheet, the above HTML is produced. This removes it.
function remove_cdae_stylesheet_link_html($html, $handle, $href = null, $media = null) {
    if($handle == 'cdae') { $html=''; }
    return $html;
}
add_filter('style_loader_tag','remove_cdae_stylesheet_link_html', 10, 4);


function _admin_html() {
    ?>
<style>
  table { border-collapse: collapse; width: 100%; }
  td { border: 1px solid silver; padding: 2px 4px; }
  table.pages td { width: 50%; }
  thead { font-weight: bold; background-color: silver; }
  ul.children { padding-left: 12px; }
  ul.list { max-height: 20rem; max-width: 100%; overflow: auto; display: block; background-color: white; position: relative; }
  ul.list { margin: 0; padding: 0; } 
  li.heading { background-color: #f1f1f1; margin: 0; padding: 4px; border-top: 1px solid silver; }
  li.heading:nth-child(1) { border: 0; }
</style>

<h2>CSS dequeue and embed or async</h2>

<p>
  Themes, some plugins, and Wordpress itself, add extra CSS to your Wordpress web pages. Wordpress provides standard methods to do this. If your plugins and themes use these standard methods, this plugin will allow you to take control over that CSS. Unfortunately if non-standard methods are used, this plugin cannot help you.
</p>

<h3>Detected CSS</h3>

<p>
  Here is a list of all the CSS detected on your site:
</p>

<table>
  <thead>
    <tr><td>ID</td><td>URL</td></tr>
  </thead>
  <tbody>
    <tr>
      <td>...</td>
      <td>...</td>
    </tr>
  </tbody>
</table>

<p>
  <h3>Pages to scan</h3>
</p>

<p>
  This plugin examines pages in your Wordpress site and notes what CSS they load. Below is a list of pages found on your Wordpress site. Select the ones you wish to examine. (Note: you probably only need to select a handful of representative pages, not all of them.)
</p>

<table class="pages">
  <thead><tr><td>Posts</td><td>Pages</td></tr></thead>
  <tbody>
    <tr>
      <td><input type="checkbox">&nbsp;Select all toggle</td>
      <td><input type="checkbox">&nbsp;Select all toggle</td>
    </tr>
    <tr>
      <td>
        <ul class="list posts">
          <?php echo get_all_archives(); ?>
        </ul>
      </td>
      <td>
        <ul class="list pages">
          <?php wp_list_pages(array('title_li' => '', 'type' => '', 'link_before' => '<input type="checkbox">&nbsp;')); ?>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

<?php
}


function get_all_archives() {
    // Irritatingly, wp_get_archives() does not get custom post types.

    $return = array();

    $args_post_types = array(
	'public' => true,
	'exclude_from_search' => false,
	'_builtin' => false
    );

    $ignore = ['page','deleted_event','attachment'];

    // FIXME wp_count_posts() below returns accurate number e.g. events, but
    // wp_get_archives() does not fetch these types.
    // Suspect the get_post_types args/names/or could fix this.

    $types = get_post_types($args_post_types,'names','or');
    foreach( $types as $type ) {
	if (! in_array($type, $ignore)) {
	    $a = wp_get_archives(array(
		'type' => 'alpha',
		'limit' => null,
		'post_type' => $type,
		'before' => '<input type="checkbox" class="cdae-checkbox">',
		'echo' => null
	    ));
	    // $return[] = '<li class="heading">Post type: ' . $type . ' (' . wp_count_posts($type)->publish . ' items found)</li>' . "\n";
	    $return[] = '<li class="heading">Post type: ' . $type . ' (' . substr_count($a,' class="cdae-checkbox"') . ' items found)</li>' . "\n";
	    $return[] = $a;
	}
    }
    return implode('',$return);
}

